# Your challenge!!

Hi there,

This challenge consist on a refactor and unit test task. The code is available in Java, Python or Javascript (nodejs). If no one has told you to choose a specific language pick the one you prefer.

Given the *send* method in the *SnippetToRefactor* class, propose a refactored, more testable, clean/S.O.L.I.D. version of the code and provide some unit tests to prove your abilities writing maintainable code.

Fork the repository in your own *Bitbucket* account, and send your solution through pull request to the branch with your name (format ${name}-${surname}, hyphen separated).

Good luck!!