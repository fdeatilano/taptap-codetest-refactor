package com.taptapnetworks.codetest;

import com.taptapnetworks.codetest.services.Sender;
import com.taptapnetworks.codetest.services.SenderFactory;

public class SnippetToRefactor {
	
	private SenderFactory senderFactory;
	
	public SnippetToRefactor(SenderFactory senderFactory){
		this.senderFactory = senderFactory;
	}
	
    public void send(String type, String message, String to, String subject) {
    	Sender sender = senderFactory.createSender(type);
    	sender.send(to, message, subject);
    }
}
