package com.taptapnetworks.codetest.services.impl;

import static com.taptapnetworks.codetest.utils.Constants.MAIL_HOST_NAME;

import com.taptapnetworks.codetest.SendMailConnection;
import com.taptapnetworks.codetest.services.Sender;

public class MailSender implements Sender {

	private SendMailConnection smc;

	public MailSender(){
		smc = new SendMailConnection(MAIL_HOST_NAME);
	}
	
	public void send(String to, String message, String subject) {
		smc.prepareMessage(to, message, subject);
        smc.send();
        smc.close();
	}

}
