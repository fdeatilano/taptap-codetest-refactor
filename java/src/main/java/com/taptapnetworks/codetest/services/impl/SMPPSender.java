package com.taptapnetworks.codetest.services.impl;

import static com.taptapnetworks.codetest.utils.Constants.SMPP_HOST_IP;
import static com.taptapnetworks.codetest.utils.Constants.SMPP_HOST_PASSWORD;
import static com.taptapnetworks.codetest.utils.Constants.SMPP_HOST_PORT;
import static com.taptapnetworks.codetest.utils.Constants.SMPP_HOST_USERNAME;

import com.taptapnetworks.codetest.SMPP;
import com.taptapnetworks.codetest.services.Sender;

public class SMPPSender implements Sender {

	private SMPP smpp;
	
	public SMPPSender(){
		smpp = new SMPP(SMPP_HOST_IP, SMPP_HOST_PORT);
	}
	public void send(String to, String message, String subject) {
        smpp.openConnection(SMPP_HOST_USERNAME, SMPP_HOST_PASSWORD);
        smpp.send(to, message);
        smpp.closeConnection();
	}

}
