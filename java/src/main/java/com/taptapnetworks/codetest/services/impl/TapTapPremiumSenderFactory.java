package com.taptapnetworks.codetest.services.impl;

import static com.taptapnetworks.codetest.utils.Constants.MAIL_MESSAGE_TYPE;
import static com.taptapnetworks.codetest.utils.Constants.SMPP_MESSAGE_TYPE;
import static com.taptapnetworks.codetest.utils.Constants.TYPE_IS_UNKNOWN;

import com.taptapnetworks.codetest.services.Sender;
import com.taptapnetworks.codetest.services.SenderFactory;

public class TapTapPremiumSenderFactory implements SenderFactory {

	public Sender createSender(String type) {
		if (type == SMPP_MESSAGE_TYPE) {
            return new SMPPSender();
        } else if (type == MAIL_MESSAGE_TYPE) {
        	return new MailSender();
        } else {
            throw new RuntimeException(TYPE_IS_UNKNOWN);
        }
	}

}
