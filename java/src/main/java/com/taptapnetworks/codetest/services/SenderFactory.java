package com.taptapnetworks.codetest.services;

public interface SenderFactory {

	/**
	 * Creates a sender of a given type
	 * @param type
	 * @return the Sender of the requested type
	 */
	Sender createSender(String type);

}
