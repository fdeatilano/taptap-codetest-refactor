package com.taptapnetworks.codetest.services;

public interface Sender {

	/**
	 * Send a message to a given destiny and with a given subject if the subject is needed according to the sending technology.
	 * 
	 * @param to Destiny of the message
	 * @param message Message to be sent
	 * @param subject Subject of the message we are sending. It is used only if he underlying technology demands it.
	 */
	void send(String to, String message, String subject);

}
