package com.taptapnetworks.codetest.utils;

public class Constants {
	
	// Message type constants
	public static final String SMPP_MESSAGE_TYPE = "SMS";
	public static final String MAIL_MESSAGE_TYPE = "mail";
	public static final String TYPE_IS_UNKNOWN = "Type is unknown";
	
	// SMPP service constants
	public static final String SMPP_HOST_IP = "192.168.1.4";
	public static final String SMPP_HOST_PORT = "4301";
	public static final String SMPP_HOST_USERNAME = "your-username";
	public static final String SMPP_HOST_PASSWORD = "your-password";
	
	// Mail service constants
	public static final String MAIL_HOST_NAME = "mail.myserver.com";

}
