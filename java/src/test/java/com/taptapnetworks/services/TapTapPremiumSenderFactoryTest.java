package com.taptapnetworks.services;

import static org.junit.Assert.*;
import static com.taptapnetworks.codetest.utils.Constants.*;

import org.junit.Before;
import org.junit.Test;

import com.taptapnetworks.codetest.services.Sender;
import com.taptapnetworks.codetest.services.impl.MailSender;
import com.taptapnetworks.codetest.services.impl.SMPPSender;
import com.taptapnetworks.codetest.services.impl.TapTapPremiumSenderFactory;

public class TapTapPremiumSenderFactoryTest {

	private TapTapPremiumSenderFactory factory;

	@Before
	public void before() {
		factory = new TapTapPremiumSenderFactory();
	}

	@Test
	public void createMailSender() throws Exception {
		Sender sender = factory.createSender(MAIL_MESSAGE_TYPE);
		assertTrue(
				"A sender of class MailSender was expected but a sender of type "
						+ sender.getClass().getName() + " was found",
				sender instanceof MailSender);
	}

	@Test
	public void createSMPPSender() throws Exception {
		Sender sender = factory.createSender(SMPP_MESSAGE_TYPE);
		assertTrue(
				"A sender of class SMPPSender was expected but a sender of type "
						+ sender.getClass().getName() + " was found",
				sender instanceof SMPPSender);
	}

	@Test
	public void createUnknownSender() throws Exception {
		try{
			factory.createSender("Unknown type");
			throw new AssertionError("A runtime exception was expected but no exception was thrown.");
		}catch(Exception e){
			assertTrue(e instanceof RuntimeException);
		}
	}
}
