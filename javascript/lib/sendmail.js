var format = require('util').format;

var SendMailConnection = function(mail_server) {
    this.mail_server = mail_server;
    console.log(format("Created SendMailConnection for server %s", mail_server));
}

SendMailConnection.prototype = {
        
    prepare_message: function(to, message, subject) {
        console.log(format("Preparing message for %s", to));
        console.log(format("Subject: %s", subject));
        console.log(message);
    },
    
    send: function() {
        console.log('Message sent!!!');
    },
    
    close: function() {
        console.log('Mail connection closed');
    }
}

module.exports = SendMailConnection;