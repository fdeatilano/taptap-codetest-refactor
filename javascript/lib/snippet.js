var Smpp = require('./smpp');
var SendMailConnection = require('./sendmail');

var SnippetToRefactor = function() {
}

SnippetToRefactor.prototype.send = function(type, message, to, subject) {
    if (type == 'SMS') {
        smpp = new Smpp('192.168.1.4', '4301')
        smpp.open('your-username', 'your-password')
        smpp.send(to, message)
        smpp.close()
    } else if (type == 'mail') {
        smc = new SendMailConnection('mail.myserver.com')
        smc.prepare_message(to, message, subject)
        smc.send()
        smc.close()
    } else {
        throw 'Invalid type';
    }
}

module.exports = SnippetToRefactor;
